import React from 'react'

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

import Home from '../Component/Home';

const Tab = createMaterialTopTabNavigator();

export default function TabBar() {
    return (
        <Tab.Navigator
            initialRouteName="Feed"
            activeColor="#00BB77"
            inactiveColor="#000000"
            shifting={true}
            barStyle={{ backgroundColor: '#FFFFFF' }}>

            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarLabel: 'HOME',
                    tabBarIcon: ({ color }) => (
                        <FontAwesome5Icon name='home' color={color} size={20} />
                    ),
                }}
            />

        </Tab.Navigator>
    )
}