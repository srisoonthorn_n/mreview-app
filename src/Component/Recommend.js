import React, { useRef, useState } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, SafeAreaView, FlatList, Image, Dimensions, } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { Card } from '@ant-design/react-native';
import Carousel from 'react-native-anchor-carousel';





export default function Recommend(props) {

    const carouselRef = useRef(null);

    const { width, height } = Dimensions.get('window');

    const [background, setBackground] = useState({

        uri: 'https://m.media-amazon.com/images/M/MV5BYWY0OGIzNzgtNTRmZS00OTc1LWFmOWItYzM0NDc4OWQ3MGI3XkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg',
        name: 'Scoob!',
        stat: '5.5',
        desc: 'Scooby and the gang face their most challenging mystery ever: a plot to unleash the ghost dog Cerberus upon the world.As they race to stop this dogpocalypse, the gang discovers that Scooby has an epic destiny greater than anyone imagined.',

    })

    const [Data, setData] = useState(
        [
            {
                id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
                title: 'Scoob!',
                date: '15 May 2020',
                img: 'https://m.media-amazon.com/images/M/MV5BYWY0OGIzNzgtNTRmZS00OTc1LWFmOWItYzM0NDc4OWQ3MGI3XkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg',
                imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
                trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo',
                description: 'Scooby and the gang face their most challenging mystery ever: a plot to unleash the ghost dog Cerberus upon the world.As they race to stop this dogpocalypse, the gang discovers that Scooby has an epic destiny greater than anyone imagined.',
                rate: '5.5'
            },
            {
                id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
                title: 'Artemis Fowl',
                date: '29 May 2020',
                img: 'https://cdn.majorcineplex.com/uploads/movie/2573/thumb_2573.jpg',
                imgbg: 'https://cdn.theouterhaven.net/wp-content/uploads/2020/03/Artemis-Fowl-Trailer.png',
                description: 'Artemis Fowl, a young criminal prodigy, hunts down a secret society of fairies to find his missing father.',
                rate: '6.0'
            },
            {
                id: '58694a0f-3da1-471f-bd96-145571e29d72',
                title: 'Dream Horse',
                date: '17 April 2020',
                img: 'https://steemitimages.com/p/6VvuHGsoU2QBt9MXeXNdDuyd4Bmd63j7zJymDTWgdcJjo1XyfYejg52JhAjoAEKd2kUaVebiNGu2RQPWrbz93TUFtbE7kqcoscc9Frm4sAY1cNK1xQqaTfCqqKGSgN?format=match&mode=fit&width=640',
                imgbg: 'https://s.yimg.com/ny/api/res/1.2/T.rewHjPw3AT1I.F9w5VGQ--~A/YXBwaWQ9aGlnaGxhbmRlcjtzbT0xO3c9ODAw/https://media.zenfs.com/en/variety.com/3c932ce704da2ca736fc6efe13c82aeb',
                description: 'Dream Alliance, an unlikely race horse bred by small town Welsh bartender, Jan Vokes. With no experience, Jan convinces her neighbors to chip in their meager earnings to help raise Dream in the hopes he can compete with the racing elites.',
                rate: '8.0'
            },
            {
                id: '58694a0f-3da1-471f-bd96-145571e29d82',
                title: 'The Wretched',
                date: '1 May 2020',
                img: 'https://lh3.googleusercontent.com/proxy/ZW-LoRu9JpU7ObR0sAl__Y9k_SS8TzSqkZgbuHOcI5OPD_ORYXl9KXKISt2hIJIGyeB-W6bVmCtA3Sv-LvflWJP0KWtfhoMm-wn6tptB',
                imgbg: 'https://i0.wp.com/bloody-disgusting.com/wp-content/uploads/2019/07/Screen-Shot-2019-07-05-at-8.38.05-AM.png?fit=1151%2C674&ssl=1',
                description: 'A defiant teenage boy, struggling with his parent\'\s imminent divorce, faces off with a thousand year-old witch, who is living beneath the skin of and posing as the woman next door.',
                rate: '7.1'
            },
            {
                id: '58694a0f-3da1-471f-bd96-145571e21d72',
                title: 'Free Guy',
                date: '11 December 2020',
                img: 'https://www.blognone.com/sites/default/files/externals/211e00c56ed01f65c46e9cfea8f30410.jpg',
                imgbg: 'https://m.media-amazon.com/images/M/MV5BMWJlZmFiNWItYTkyMy00NDE3LWI1ZWEtZjEwNTIxYWZjODQ1XkEyXkFqcGdeQW1hcmNtYW5u._V1_.jpg',
                description: 'A bank teller discovers that he\'\s actually an NPC inside a brutal, open world video game.',
                rate: '8.5'
            },
            {
                id: '58694a0f-3da1-471f-bd96-145571e21gf2',
                title: 'Coffee & Kareem',
                date: ' 3 April 2020',
                img: 'https://www.movie123hd.com/wp-content/uploads/2020/04/tt9898858-scaled.jpg',
                imgbg: 'https://www.indiewire.com/wp-content/uploads/2020/04/Coffee-Kareem-3.jpg',
                description: 'Twelve-year-old Kareem Manning hires a criminal to scare his mom\'\s new boyfriend -police officer James Coffee - but it backfires, forcing Coffee and Kareem to team up in order to save themselves from Detroit\'\s most ruthless drug kingpin.',
                rate: '6.4'
            },
        ]
    );



    const Movie = ({ item, index }) => {
        return (
            <View>
                <TouchableOpacity onPress={() => {
                    carouselRef.current.scrollToIndex(index);
                    setBackground({
                        uri: item.img,
                        name: item.title,
                        stat: item.rate,
                        desc: item.description,
                    })
                }}>
                    <Image source={{ uri: item.img }} style={styles.carouselImage} />
                    <View style={{ marginTop: 10, padding: 5, position: 'relative' }}>
                        <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 16, fontWeight: 'bold' }}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    return (

        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }} >

            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 5, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Text>LOGO</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>

            </View>

            <View style={{ height: 40, backgroundColor: '#001F2D', flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Text style={{ color: '#9F9F9F' }}>HOME</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Movie')}>
                        <Text style={{ color: '#9F9F9F' }}>MOVIE</Text>
                    </TouchableOpacity>

                </View>
                <View style={[styles.itemMenuRecommend, styles.itemSelect]}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Recommend')}>
                        <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>RECOMMENDATIONS</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ height: 550, width: '100%', backgroundColor: '#001F2D', marginBottom: 10 }}>
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Recommended For You</Text>
                </View>

                {/* <SafeAreaView style={styles.container}> */}
                {/* <FlatList
                        data={DATA}
                        horizontal={true}
                        renderItem={({ item }) => (
                            <Card style={[styles.cardItem]}>

                                <Image
                                    style={{ width: '100%', height: 210 }}
                                    source={{
                                        uri: item.img,
                                    }}
                                />
                                <View style={[styles.cardItemTital, styles.cardItemSelect]}>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: 'bold' }}>{item.title}</Text>
                                    </View>
                                </View>
                            </Card>
                        )}
                        keyExtractor={item => item.id}
                    /> */}



                {/* </SafeAreaView> */}

                {/* <View style={{ height: 135, marginTop: 10, padding: 5 }}>
                    <View style={{ flexDirection: 'row' }}>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 18, fontWeight: 'bold' }}>Scoob</Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Icon name='star' size={20} color='#F4BC00' solid> 5.5</Icon>
                        </View>

                    </View>
                    <View style={{ width: '90%', marginTop: 5 }}>
                        <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 12, padding: 5 }}>Set in a future where consciousness is digitized and stored, a prisoner returns to life in a new body and must solve a mind-bending murder to win his freedom.</Text>
                    </View>

                </View> */}

                <View style={styles.carouselContainerView}>
                    <Carousel

                        style={styles.Carousel}
                        data={Data}
                        renderItem={Movie}
                        itemWidth={200}
                        containerWidth={width - 20}
                        separatorWidth={0}
                        ref={carouselRef}
                        inActiveOpacity={0.4}
                    />
                </View>

                <View style={styles.movieInfoContainer}>
                    <View style={{ justifyContent: 'center', flex: 1 }}>
                        <Text style={styles.movieName}>{background.name}</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Icon name='star' size={20} color='#F4BC00' solid>{background.stat}</Icon>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 14, marginTop: 14 }}>
                    <Text style={styles.movieDis} numberOfLines={4}>{background.desc}</Text>
                </View>

            </View>

            <View style={{ backgroundColor: '#001F2D', width: '100%', height: 547, marginBottom: 10 }}>
                <View style={{ justifyContent: 'center', marginBottom: 5 }}>
                    <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Catagories Movies and TV Shows</Text>
                </View>

                <TouchableOpacity onPress={() => props.navigation.navigate('Movie_List')} style={{ marginBottom: 10 }}>
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', opacity: 42, width: '100%', height: 55, alignItems: 'center', flexDirection: 'row' }}>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 18 }}>Popular Movies</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', marginEnd: 40 }}>
                            <Icon name='chevron-right' size={20} color='#FFFFFF'></Icon>
                        </View>

                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => props.navigation.navigate('Movie_List')} style={{ marginBottom: 10 }}>
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', opacity: 50, width: '100%', height: 55, alignItems: 'center', flexDirection: 'row' }}>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 18 }}>Popular TV</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', marginEnd: 40 }}>
                            <Icon name='chevron-right' size={20} color='#FFFFFF'></Icon>
                        </View>

                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ marginBottom: 10 }}>
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', opacity: 50, width: '100%', height: 55, alignItems: 'center', flexDirection: 'row' }}>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 18 }}>Popular Horror</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', marginEnd: 40 }}>
                            <Icon name='chevron-right' size={20} color='#FFFFFF'></Icon>
                        </View>

                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ marginBottom: 10 }}>
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', opacity: 50, width: '100%', height: 55, alignItems: 'center', flexDirection: 'row' }}>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 18 }}>Popular Comedy</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', marginEnd: 40 }}>
                            <Icon name='chevron-right' size={20} color='#FFFFFF'></Icon>
                        </View>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ marginBottom: 10 }}>
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', opacity: 50, width: '100%', height: 55, alignItems: 'center', flexDirection: 'row' }}>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 18 }}>Popular Drama</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', marginEnd: 40 }}>
                            <Icon name='chevron-right' size={20} color='#FFFFFF'></Icon>
                        </View>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ marginBottom: 10 }}>
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', opacity: 50, width: '100%', height: 55, alignItems: 'center', flexDirection: 'row' }}>

                        <View style={{ flex: 1 }}>
                            <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 18 }}>Popular Sci-Fi</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', marginEnd: 40 }}>
                            <Icon name='chevron-right' size={20} color='#FFFFFF'></Icon>
                        </View>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ marginBottom: 10 }}>
                    <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', opacity: 50, width: '100%', height: 55, alignItems: 'center', flexDirection: 'row' }}>

                        <View style={{ flex: 1.5 }}>
                            <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 18 }}>Popular Romance</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end', marginEnd: 40 }}>
                            <Icon name='chevron-right' size={20} color='#FFFFFF'></Icon>
                        </View>

                    </View>
                </TouchableOpacity>
            </View>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemMenuRecommend: {
        flex: 1.5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#F3B502'
    },
    cardItem: {
        width: 150,
        height: 263,
        marginLeft: 10,
        marginRight: 10,
    },
    cardItemSelect: {
        borderBottomWidth: 5,
        borderColor: '#023A54'
    },
    cardItemTital: {
        height: 50,
        alignItems: 'center',
        backgroundColor: '#161616'
    },
    carouselImage: {

    },
    carouselContainerView: {
        width: '100%',
        height: 350,
        justifyContent: 'center',
        alignItems: 'center'
    },
    Carousel: {
        flex: 1,
        overflow: 'visible'
    },
    carouselImage: {
        width: 200,
        height: 320,
        borderRadius: 10,
        alignSelf: 'center',
        backgroundColor: 'rgba(0,0,0,0.9)'
    },
    movieInfoContainer: {
        flexDirection: 'row',
        marginTop: 16,
        justifyContent: 'space-between',
        width: Dimensions.get('window').width - 14
    },
    movieName: {
        paddingLeft: 14,
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 6
    },
    movieDis: {
        color: '#FFFFFF',
        opacity: 0.8,
        lineHeight: 20,
        fontSize: 12
    }
});