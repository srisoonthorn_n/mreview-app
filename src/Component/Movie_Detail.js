import React, { useState } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, ImageBackground, Image, SafeAreaView, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { Card } from '@ant-design/react-native';

const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad4abb28ba',
        title: 'Scoob!',
        date: '15 May 2020',
        img: 'https://lady3sha.files.wordpress.com/2020/03/scoob-movie-5.jpg?w=780',
        imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
        trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo'
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53ab38ba',
        title: 'Scoob!',
        date: '15 May 2020',
        img: 'https://img.online-station.net/_content/2019/1110/149779/gallery/0402_700_293.jpg',
        imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
        trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo'
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad13ab128ba',
        title: 'Scoob!',
        date: '15 May 2020',
        img: 'https://i0.wp.com/icantunseethatmovie.com/wp-content/uploads/2019/11/Scoob_ShaggyScooby.jpg?ssl=1',
        imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
        trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo'
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Scoob!',
        date: '15 May 2020',
        img: 'https://i2.wp.com/secureservercdn.net/ip-ac.mwp2.iad2.godaddy.com/502.53c.godaddywp.com/wp-content/uploads/2020/03/img_7557.jpg?resize=854%2C540&ssl=1',
        imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
        trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo'
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53ab18ba',
        title: 'Scoob!',
        date: '15 May 2020',
        img: 'https://meiahora.ig.com.br/_midias/jpg/2020/03/06/700x470/1_2-16105403.jpg',
        imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
        trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo'
    },
];

export default function Moive_Detail(props) {

    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#0C0C0C' }} contentContainerStyle={{ flexGrow: 1 }} >

            <View style={{ height: 40, backgroundColor: '#FFFFFF', flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Text>LOGO</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ height: 240, width: '100%' }}>
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={DATA}
                        horizontal={true}
                        renderItem={({ item }) => (
                            <View style={{ height : 300 , width : 411 }}>
                                <Image source={{ uri: item.img }} style={{ width: '100%', height: 300 }} />
                            </View>
                        )}
                        keyExtractor={item => item.id}
                    />
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <View style={{ width: '100%', height: 64, backgroundColor: 'rgba(0,0,0,0.7)' }}>
                            <View style={{ justifyContent: 'center' }}>
                                <Text style={{ fontSize: 30, color: '#FFFFFF', marginStart: 20, fontWeight: 'bold' }}>Scoob!</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Text style={{ fontSize: 13, color: '#AEAEAE', marginStart: 20 }}>PG</Text>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>

            </View>
            <View style={{ height: 180, width: '100%', backgroundColor: '#0C0C0C', flexDirection: 'row' }}>
                <View style={{ flex: 0, margin: 10 }}>
                    <Image source={{ uri: 'https://m.media-amazon.com/images/M/MV5BYWY0OGIzNzgtNTRmZS00OTc1LWFmOWItYzM0NDc4OWQ3MGI3XkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg' }}
                        style={{ width: 105, height: 156 }}></Image>
                </View>
                <View style={{ flex: 1, backgroundColor: '#000000', marginBottom: 10, marginTop: 10 }}>
                    <View style={{ width: '100%', height: 30, flexDirection: 'row' }}>
                        <View style={{ flex: 1, borderWidth: 1, borderColor: '#FFFFFF', margin: 4, alignItems: 'center' }}>
                            <Text style={{ color: '#FFFFFF', justifyContent: 'center' }}>Animation</Text>
                        </View>
                        <View style={{ flex: 1, borderWidth: 1, borderColor: '#FFFFFF', margin: 4, alignItems: 'center' }}>
                            <Text style={{ color: '#FFFFFF', justifyContent: 'center' }}>Adventure</Text>
                        </View>
                        <View style={{ flex: 1, borderWidth: 1, borderColor: '#FFFFFF', margin: 4, alignItems: 'center' }}>
                            <Text style={{ color: '#FFFFFF', justifyContent: 'center' }}>Comedy</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, margin: 5  , padding : 2}}>
                        <Text style={{ color: '#FFFFFF', fontSize: 12 }}>Scooby and the gang face their most challenging mystery ever: a plot to unleash the ghost dog Cerberus upon the world.
                        As they race to stop this dogpocalypse, the gang discovers that Scooby has an epic destiny greater than anyone imagined.
                        </Text>
                    </View>
                </View>
            </View>

            <View style={{ height: 60, width: '100%', backgroundColor: '#FFFFFF', marginBottom: 14 }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={{ color: '#000000', margin: 10, fontSize: 12, fontWeight: 'bold' }}>Coming Soon</Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={{ color: '#000000', margin: 10, fontSize: 12, fontWeight: 'bold' }}>15 May 2020</Text>
                </View>
            </View>

            <View style={{ height: 100, width: '100%', backgroundColor: '#001F2D', marginBottom: 14 }}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>

                    <TouchableOpacity onPress={() => props.navigation.navigate('User_Review')} style={{ flex: 1 }}>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <Icon name='star' size={24} color='#F4BC00' solid></Icon>
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>7.0</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 14, color: '#FFFFFF' }}> / 10</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={() => props.navigation.navigate('Write_Review')} style={{ flex: 1 }}>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <Icon name='star' size={25} color='#FFFFFF' regular></Icon>
                            <View>
                                <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>RATE THIS</Text>
                            </View>
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity style={{ flex: 1 }}>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <View style={{ borderColor: '#007C32', borderWidth: 3, backgroundColor: '#007C32' }}>
                                <Text style={{ fontSize: 14, color: '#FFFFFF', }}>68</Text>
                            </View>
                            <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>Metascore</Text>

                        </View>
                    </TouchableOpacity>

                </View>
            </View>

            <View style={{ height: 100, width: '100%', backgroundColor: '#001F2D', marginBottom: 14 }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 16, fontWeight: 'bold' }}>User Review</Text>
                    </View>

                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                        <TouchableOpacity onPress={() => props.navigation.navigate('User_Review')}>
                            <Text style={{ color: '#F3B502', margin: 10, fontSize: 15, fontWeight: 'bold' }}>SEE ALL</Text>
                        </TouchableOpacity>

                    </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Icon name='star' size={16} color='#F4BC00' solid></Icon>
                    </View>

                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 5, marginEnd: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text style={{ fontSize: 14, color: '#F3B502', fontWeight: 'bold' }}>7</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 14, color: '#FFFFFF' }}> / 10</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>WOW I say scooby doo bee doo!</Text>
                    </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Icon name='star' size={16} color='#F4BC00' solid></Icon>
                    </View>

                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 5, marginEnd: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text style={{ fontSize: 14, color: '#F3B502', fontWeight: 'bold' }}>8</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 14, color: '#FFFFFF' }}> / 10</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>I can't wait to see it.</Text>
                    </View>
                </View>
            </View>

            <View style={{ height: 280, width: '100%', backgroundColor: '#001F2D', marginBottom: 14 }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 16, fontWeight: 'bold' }}>Image</Text>
                    </View>

                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                        <TouchableOpacity onPress={() => props.navigation.navigate('Photo_List')}>
                            <Text style={{ color: '#F3B502', margin: 10, fontSize: 15, fontWeight: 'bold' }}>SEE ALL</Text>
                        </TouchableOpacity>

                    </View>
                </View>

                <View style={{ height: 200, width: '100%' }}>
                    <SafeAreaView style={styles.container}>
                        <FlatList
                            data={DATA}
                            horizontal={true}
                            renderItem={({ item }) => (
                                <TouchableOpacity  onPress={() => props.navigation.navigate('Photo_Show')}>
                                    <Card style={{ width: 317, height: 180, marginLeft: 10, marginRight: 10, }}>
                                        <Image
                                            style={{ width: '100%', height: 179 }}
                                            source={{
                                                uri: item.img,
                                            }}
                                        />
                                    </Card>
                                </TouchableOpacity>

                            )}
                            keyExtractor={item => item.id}
                        />
                    </SafeAreaView>

                </View>

            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#001F2D'
    }
});

