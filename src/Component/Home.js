import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView, FlatList, SafeAreaView, StyleSheet, Image, TouchableOpacity, ImageBackground, TouchableNativeFeedback, TouchableWithoutFeedback, Animated } from 'react-native'
import { Card, WingBlank } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import AppIntroSlider from 'react-native-app-intro-slider';
import axios from 'axios';
import { listAllNews } from '../api';


const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Scoob!',
        date: '15 May 2020',
        img: 'https://m.media-amazon.com/images/M/MV5BYWY0OGIzNzgtNTRmZS00OTc1LWFmOWItYzM0NDc4OWQ3MGI3XkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg',
        imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
        trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo'
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Artemis Fowl',
        date: '29 May 2020',
        img: 'https://cdn.majorcineplex.com/uploads/movie/2573/thumb_2573.jpg',
        imgbg: 'https://cdn.theouterhaven.net/wp-content/uploads/2020/03/Artemis-Fowl-Trailer.png'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Dream Horse',
        date: '17 April 2020',
        img: 'https://steemitimages.com/p/6VvuHGsoU2QBt9MXeXNdDuyd4Bmd63j7zJymDTWgdcJjo1XyfYejg52JhAjoAEKd2kUaVebiNGu2RQPWrbz93TUFtbE7kqcoscc9Frm4sAY1cNK1xQqaTfCqqKGSgN?format=match&mode=fit&width=640',
        imgbg: 'https://s.yimg.com/ny/api/res/1.2/T.rewHjPw3AT1I.F9w5VGQ--~A/YXBwaWQ9aGlnaGxhbmRlcjtzbT0xO3c9ODAw/https://media.zenfs.com/en/variety.com/3c932ce704da2ca736fc6efe13c82aeb'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d82',
        title: 'The Wretched',
        date: '1 May 2020',
        img: 'https://lh3.googleusercontent.com/proxy/ZW-LoRu9JpU7ObR0sAl__Y9k_SS8TzSqkZgbuHOcI5OPD_ORYXl9KXKISt2hIJIGyeB-W6bVmCtA3Sv-LvflWJP0KWtfhoMm-wn6tptB',
        imgbg: 'https://i0.wp.com/bloody-disgusting.com/wp-content/uploads/2019/07/Screen-Shot-2019-07-05-at-8.38.05-AM.png?fit=1151%2C674&ssl=1'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e21d72',
        title: 'Free Guy',
        date: '11 December 2020',
        img: 'https://www.blognone.com/sites/default/files/externals/211e00c56ed01f65c46e9cfea8f30410.jpg',
        imgbg: 'https://m.media-amazon.com/images/M/MV5BMWJlZmFiNWItYTkyMy00NDE3LWI1ZWEtZjEwNTIxYWZjODQ1XkEyXkFqcGdeQW1hcmNtYW5u._V1_.jpg'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e21gf2',
        title: 'Coffee & Kareem',
        date: ' 3 April 2020',
        img: 'https://www.movie123hd.com/wp-content/uploads/2020/04/tt9898858-scaled.jpg',
        imgbg: 'https://www.indiewire.com/wp-content/uploads/2020/04/Coffee-Kareem-3.jpg'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571f23gf2',
        title: 'Valley Girl 2020',
        date: ' 2020',
        img: 'https://lh3.googleusercontent.com/proxy/S3ONowp8Z7LP_E2AtT98W2iNZ-jAsdiyEZtQ73Cjm8r2ry78S-8mZ_HnlAOOvd5U7NftcKZkNqSSIbG_Z6OQbA0WWPUsSLeokH60Mp8',
        imgbg: 'https://video.newsserve.net/v/20200416/1306370628-VALLEY-GIRL-movie-2020-Jessica-Rothe-Josh_hires.jpg',
        trailer: 'https://static0.srcdn.com/wordpress/wp-content/uploads/2020/04/Julie-and-Randy-in-Valley-Girl.jpg?q=50&fit=crop&w=960&h=500'
    },
];

const News = [
    {
        id: '1',
        title: 'Wonder Woman 1984 Will Maintain Batman V Superman Continuity',
        shotText: 'A brand new feature on Patty Jenkins \'\ Wonder Woman 1984 reveals that the film will maintain the continuity set for Diana in Batman V Superman.',
        img: 'https://static1.srcdn.com/wordpress/wp-content/uploads/2017/06/Wonder-Woman-Movie-Ending-BvS-Difference.jpg?q=50&fit=crop&w=943&h=500'
    },
    {
        id: '2',
        title: 'Star Wars: Kylo Leads the Knights of Ren in Rise of Skywalker Animated Short',
        shotText: 'A new animated Star Wars: The Rise of Skywalker short features Kylo Ren leading the Knights of Ren in an attack on Resistance fighters.',
        img: 'https://static0.srcdn.com/wordpress/wp-content/uploads/2020/04/Kylo-Ren-and-the-Knights-of-Ren-in-Star-Wars-Animated-Short.jpg?q=50&fit=crop&w=960&h=500'
    },
    {
        id: '3',
        title: 'Kick-Ass Almost Starred Brad Pitt as Big Daddy Instead of Nic Cage',
        shotText: 'Kick-Ass director Matthew Vaughn has revealed that he originally had Brad Pitt in mind to play the role of Big Daddy and not Nicolas Cage.',
        img: 'https://static2.srcdn.com/wordpress/wp-content/uploads/2020/04/Brad-Pitt-Almost-Starred-As-Big-Daddy-in-Kick-Ass-Instead-of-Nic-Cage.jpg?q=50&fit=crop&w=960&h=500'
    },
    {
        id: '4',
        title: 'Bane Masks Sell Out Online Thanks To Batman Fans',
        shotText: 'As masks become law in some cities due to the COVID-19 pandemic, Batman fans have begun snapping up replica Bane masks in an attempt at staying safe.',
        img: 'https://static1.srcdn.com/wordpress/wp-content/uploads/Bane-in-The-Dark-Knight-Rises2.jpg?q=50&fit=crop&w=960&h=500'
    },

]





export default function Home(props) {

    const [news, setNews] = useState([]);

    useEffect(() => {
        // axios.get("http://192.168.1.6:8080/news")
        listAllNews()
            .then((response) => {
                setNews(response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }, []);

    return (

        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }} >

            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 5, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Text>LOGO</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>

            </View>

            <View style={{ height: 40, backgroundColor: '#001F2D', flexDirection: 'row', marginBottom: 10 }}>
                <View style={[styles.itemMenu, styles.itemSelect]}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Text style={{ fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' }}>HOME</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Movie')}>
                        <Text style={{ color: '#9F9F9F' }}>MOVIE</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Recommend')}>
                        <Text style={{ color: '#9F9F9F' }}>RECOMMENDATIONS</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ flex: 1 }}>
                {/* <View style={{ width: '100%', height: 272, backgroundColor: '#001F2D', marginBottom: 10 }}>
                    <View style={{ width: '100%', height: 220 }}>
                        <ImageBackground source={{ uri: DATA[6].trailer }} style={{ flex: 1 }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('Video')}>
                                    <Icon name='play-circle' size={64} color='#FFFFFF' light />
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={{ width: '100%', height: 56, padding: 10 }}>
                        <Text style={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: 14 }}>Valley Girl Trailer: Musical Remake Heads Straight to Digital in May</Text>
                    </View>
                </View> */}

                {
                    news.map((item, index) => {
                        return (
                            <View style={{ width: '100%', height: 400, backgroundColor: '#001F2D', marginBottom: 10 }}  >
                                <View style={{ width: '100%', height: 64, padding: 10 }}>
                                    <Text style={{ fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' }}>{item.header_news}</Text>
                                </View>

                                <View style={{ width: '100%', height: 200, paddingLeft: 10, paddingRight: 10 }}>
                                    <Image style={{ width: '100%', height: 200 }} source={{ uri: item.image, }} key={index} />
                                </View>
                                <View style={{ width: '100%', height: 48, padding: 10 }}>
                                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{item.shot_detail}</Text>
                                </View>

                                <View style={{ flex: 1, justifyContent: 'center', padding: 10 }}>
                                    <TouchableOpacity>
                                        <Text style={{ color: '#F3B502', fontSize: 14, fontWeight: 'bold' }}> SEE MORE</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        )
                    })
                }


                {/* <View style={{ width: '100%', height: 400, backgroundColor: '#001F2D', marginBottom: 10 }} >
                    <View style={{ width: '100%', height: 64, padding: 10 }}>
                        <Text style={{ fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' }}>{News[1].title}</Text>
                    </View>

                    <View style={{ width: '100%', height: 200, paddingLeft: 10, paddingRight: 10 }}>
                        <Image style={{ width: '100%', height: 200 }} source={{ uri: News[1].img, }} />
                    </View>
                    <View style={{ width: '100%', height: 48, padding: 10 }}>
                        <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{News[1].shotText}</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 10 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#F3B502', fontSize: 14, fontWeight: 'bold' }}> SEE MORE</Text>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={{ width: '100%', height: 400, backgroundColor: '#001F2D', marginBottom: 10 }} >
                    <View style={{ width: '100%', height: 64, padding: 10 }}>
                        <Text style={{ fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' }}>{News[2].title}</Text>
                    </View>

                    <View style={{ width: '100%', height: 200, paddingLeft: 10, paddingRight: 10 }}>
                        <Image style={{ width: '100%', height: 200 }} source={{ uri: News[2].img, }} />
                    </View>
                    <View style={{ width: '100%', height: 48, padding: 10 }}>
                        <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{News[2].shotText}</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 10 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#F3B502', fontSize: 14, fontWeight: 'bold' }}> SEE MORE</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ width: '100%', height: 400, backgroundColor: '#001F2D', marginBottom: 10 }} >
                    <View style={{ width: '100%', height: 64, padding: 10 }}>
                        <Text style={{ fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' }}>{News[3].title}</Text>
                    </View>

                    <View style={{ width: '100%', height: 200, paddingLeft: 10, paddingRight: 10 }}>
                        <Image style={{ width: '100%', height: 200 }} source={{ uri: News[3].img, }} />
                    </View>
                    <View style={{ width: '100%', height: 48, padding: 10 }}>
                        <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{News[3].shotText}</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 10 }}>
                        <TouchableOpacity onPress={() => props.navigation.navigate('News_Detail')}>
                            <Text style={{ color: '#F3B502', fontSize: 14, fontWeight: 'bold' }}> SEE MORE</Text>
                        </TouchableOpacity>
                    </View>
                </View> */}
            </View>



            <View style={{ height: 350, backgroundColor: '#001F2D', marginBottom: 20 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Coming Soon</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#F3B502', margin: 10, fontSize: 15, fontWeight: 'bold' }}>SEE ALL</Text>
                        </TouchableOpacity>

                    </View>

                </View>

                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={DATA}
                        horizontal={true}
                        renderItem={({ item }) => (
                            <Card style={{ width: 180, height: 280, marginLeft: 10, marginRight: 10, }}>
                                {/* <Card.Header
                                    thumb ={item.img}
                                    thumbStyle={{width: 170, height: 250 }} 
                                    /> */}
                                <Image
                                    style={{ width: '100%', height: 208 }}
                                    source={{
                                        uri: item.img,
                                    }}
                                />
                                <View style={{ height: 70, paddingLeft: 10, backgroundColor: '#161616' }}>
                                    <View style={{ flex: 1, padding: 5 }}>
                                        <Text style={{ color: '#FFFFFF', fontSize: 18, fontWeight: 'bold' }}>{item.title}</Text>
                                    </View>
                                    <View style={{ flex: 1, padding: 5 }}>
                                        <Text style={{ color: '#FFFFFF', fontSize: 12, }}>{item.date}</Text>
                                    </View>
                                </View>


                            </Card>
                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>

            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#F3B502'
    },
    flatListStyles: {
        flex: 1,
        backgroundColor: "#001F2D",
        padding: 20
    }
});