import React, { useState } from 'react'
import { View, TouchableNativeFeedback, TouchableWithoutFeedback, Text, StyleSheet } from 'react-native'

import ProgressBar from "react-native-progress/Bar";
import Video from 'react-native-video';
import Scoob from '../../Video/SCOOB.mp4';
import VideoPlayer from 'react-native-video-controls';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';

export default function VideoPlayerView(props){
    const [paused, setPaused] = useState(false);
    const [duration] = useState(0);
    const [progress, setProgress] = useState(0);

    const secondsToTime = (time) => {
        return ~~(time / 60) + ':' + (time % 60 < 10 ? '0' : '') + time % 60;
    }

    const handleMainButtonTouch = () => {
        if (progress >= 1) {
            player.seek(0);
        }
        setPaused(!paused);
    };

    const handleProgressPress = e => {
        const position = e.nativeEvent.locationX;
        const progress = (position / 250) * duration;
        const isPlaying = !paused;

        player.seek(progress);
    };

    const handleProgress = progress => {
        setProgress(progress.currentTime / duration);
    };

    const handleEnd = () => {
        setPaused(true);
    };

    return(
   <View style={{flex :1 , backgroundColor : '#001F2D'}}>
            <Video
                source={Scoob}
                paused={paused}
                style={{ width: '100%', height: 441 }}
                onProgress={handleProgress}
                onEnd={handleEnd}
                ref={ref => {
                    player = ref;
                  }}
            />
            <View style={{
                backgroundColor: 'rgba(0,0,0,0.5)', height: 48, left: 0, bottom: 0,
                right: 0, position: 'relative', flexDirection: 'row', alignItems: 'center',
                justifyContent: 'space-around', paddingHorizontal: 10
            }}>
                <TouchableNativeFeedback onPress={handleMainButtonTouch}>
                    <Icon name={!paused ? 'pause' : 'play'} size={24} color="#FFFFFF"></Icon>
                </TouchableNativeFeedback>

                {/* <TouchableWithoutFeedback onPress={handleProgressPress}>
              <View>
                <ProgressBar
                  progress={progress}
                  color='#FFFFFF'
                  unfilledColor='rgba(255,255,255,0.5)'
                  borderColor="#FFF"
                  width={250}
                  height={20}
                />
              </View>
            </TouchableWithoutFeedback> */}

            <Text style={styles.duration}>
              {secondsToTime(Math.floor(progress * duration))}
            </Text>

            </View>
            </View> 
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    duration: {
        color: "#FFF",
        marginLeft: 15,
      },
});