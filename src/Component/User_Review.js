import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function User_Review(props) {
    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 10, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>User Review</Text>
                </View>

            </View>

            <View style={{ height: 56 }}>
                <View style={{ flex: 1, padding: 10 }}>
                    <View style={{ width: '100%', height: 30, flexDirection: 'row', }}>
                        <View style={{ width: 78, height: 30, backgroundColor: '#FFFFFF', margin: 4, alignItems: 'center', justifyContent: 'center', borderRadius: 8 }}>
                            <Text style={{ color: '#000000', justifyContent: 'center', fontSize: 14, fontWeight: 'bold' }}>Thrilling</Text>
                        </View>
                        <View style={{ width: 78, height: 30, backgroundColor: '#BABABA', margin: 4, alignItems: 'center', justifyContent: 'center', borderRadius: 8 }}>
                            <Text style={{ color: '#000000', justifyContent: 'center', fontSize: 14, fontWeight: 'bold' }}>Excited</Text>
                        </View>
                        <View style={{ width: 78, height: 30, backgroundColor: '#BABABA', margin: 4, alignItems: 'center', justifyContent: 'center', borderRadius: 8 }}>
                            <Text style={{ color: '#000000', justifyContent: 'center', fontSize: 14, fontWeight: 'bold' }}>Strange</Text>
                        </View>
                    </View>
                </View>

            </View>

            <View style={{ backgroundColor: '#001F2D', width: '100%', height: 200, marginBottom: 10 }}>

                <View style={{ flexDirection: 'row', }}>
                    <View style={{ padding: 10, marginStart: 7 }}>
                        <Icon name='user-circle' size={24} color='#FFFFFF' solid></Icon>
                    </View>

                    <View style={{ padding: 10 }}>
                        <Text style={{ color: '#FFFFFF', fontSize: 14, fontWeight: 'bold' }}>User 1</Text>
                    </View>


                    <View style={{ flex: 1, padding: 10, marginEnd: 20, alignItems: 'flex-end' }}>
                        <Text style={{ color: '#AEAEAE' }}>19/04/2020</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Icon name='star' size={16} color='#F4BC00' solid></Icon>
                    </View>

                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 5, marginEnd: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text style={{ fontSize: 14, color: '#F3B502', fontWeight: 'bold' }}>7</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 14, color: '#FFFFFF' }}> / 10</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>Hate it but not cause it is bad.</Text>
                    </View>
                </View>

                <View style={{ flex: 1, padding: 15 }}>
                    <Text style={{ color: '#FFFFFF' }}>
                        I hated this movie because it hit me so hard. Lost my dad a while back and never dealt with all the things from that but this movie kinda drop kicked my feelings into the focus.
                        Hard to watch a film with tears streaming down your face....
                    </Text>
                </View>

            </View>

            <View style={{ backgroundColor: '#001F2D', width: '100%', height: 200, marginBottom: 10 }}>

                <View style={{ flexDirection: 'row', }}>
                    <View style={{ padding: 10, marginStart: 7 }}>
                        <Icon name='user-circle' size={24} color='#FFFFFF' solid></Icon>
                    </View>

                    <View style={{ padding: 10 }}>
                        <Text style={{ color: '#FFFFFF', fontSize: 14, fontWeight: 'bold' }}>User 2</Text>
                    </View>


                    <View style={{ flex: 1, padding: 10, marginEnd: 20, alignItems: 'flex-end' }}>
                        <Text style={{ color: '#AEAEAE' }}>19/04/2020</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Icon name='star' size={16} color='#F4BC00' solid></Icon>
                    </View>

                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 5, marginEnd: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text style={{ fontSize: 14, color: '#F3B502', fontWeight: 'bold' }}>9</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 14, color: '#FFFFFF' }}> / 10</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>Great film that doesn't have issues of other films.</Text>
                    </View>
                </View>

                <View style={{ flex: 1, padding: 15 }}>
                    <Text style={{ color: '#FFFFFF' }}>
                        The dead parent trope might be overused for Disney movies but Onward doesn't make the mistakes of those movies.
                        The siblings fight like siblings though out, not just at plot critical moments...
                    </Text>
                </View>

            </View>


            <View style={{ backgroundColor: '#001F2D', width: '100%', height: 200, marginBottom: 10 }}>

                <View style={{ flexDirection: 'row', }}>
                    <View style={{ padding: 10, marginStart: 7 }}>
                        <Icon name='user-circle' size={24} color='#FFFFFF' solid></Icon>
                    </View>

                    <View style={{ padding: 10 }}>
                        <Text style={{ color: '#FFFFFF', fontSize: 14, fontWeight: 'bold' }}>User 3</Text>
                    </View>


                    <View style={{ flex: 1, padding: 10, marginEnd: 20, alignItems: 'flex-end' }}>
                        <Text style={{ color: '#AEAEAE' }}>19/04/2020</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Icon name='star' size={16} color='#F4BC00' solid></Icon>
                    </View>

                    <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 5, marginEnd: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text style={{ fontSize: 14, color: '#F3B502', fontWeight: 'bold' }}>7</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 14, color: '#FFFFFF' }}> / 10</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                        <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>Movie for boys where the price is love - bravo Pixar.</Text>
                    </View>
                </View>

                <View style={{ flex: 1, padding: 15 }}>
                    <Text style={{ color: '#FFFFFF' }}>
                        Doing a fulfilling movie for boys where the price of the quest is emotional wholeness and found love is a rare sight and not an easy task...
                    </Text>
                </View>

            </View>

            <TouchableOpacity onPress={() => props.navigation.navigate('Write_Review')}>
                <View style={{ flex: 1, alignItems: 'flex-end', padding: 10 }}>
                    <Icon name='pen-square' size={56} color='#FFFFFF' solid></Icon>
                </View>
            </TouchableOpacity>





        </ScrollView>
    )
}