import React from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';

export default function News_Detail(props) {

    const DATA = [
        {
            id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
            title: 'Bane Masks Sell Out Online Thanks To Batman Fans',
            date: 'MAY 19, 2020',
            writeBy: 'MIKE JONES',
            img: 'https://static1.srcdn.com/wordpress/wp-content/uploads/Bane-in-The-Dark-Knight-Rises2.jpg?q=50&fit=crop&w=960&h=500',
            description1: 'As masks become law in some cities due to the COVID-19 pandemic, Batman fans have begun snapping up replica Bane masks in an attempt at staying safe.',
            description2: 'Bane masks are selling out online thanks to Batman fans.The character was a hit on the big screen back in 2012, thanks in no small part to Tom Hardy’s portrayal of the notorious villain in the final installment of Christopher Nolan’s Batman trilogy.',
            description3: 'Bane struck a formidable presence in comic books before he was truly brought to life by Hardy in The Dark Knight Rises. And while the comic book version of the supervillain does appear considerably larger and more brutish than Hardy’s version, the performance left many fans riveted.Bane’s look has since become instantly recognizable – not just for fans of Nolan’s series, either, but within pop culture as a whole. What’s more, with the current state of the world what it is due to the coronavirus, it seems the masked approach of many superheroes (and supervillains) has actually become a sensible one. In fact, in some places, wearing a mask has become a legal requirement when leaving the house, ensuring that what was once a method to keep a hero or villain’s identity hidden is now a method of maintaining health.',
            link: 'https://screenrant.com/batman-bane-masks-sell-out-online-coronavirus/'
        }
    ];

    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 14, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Text>LOGO</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ height: 122, backgroundColor: '#001F2D', marginBottom: 14 }}>
                <View style={{ flex: 1, padding: 10 }}>
                    <Text style={{ color: '#FFFFFF', fontSize: 24, fontWeight: 'bold' }}>{DATA[0].title}</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 10 }}>
                    <Text style={{ color: '#A4A4A4', fontSize: 12 }}>BY {DATA[0].writeBy}</Text>
                    <Text style={{ color: '#A4A4A4', fontSize: 12 }}>{DATA[0].date}</Text>
                </View>
            </View>

            <View style={{ height: 225, backgroundColor: '#001F2D', marginBottom: 14, padding: 10 }}>
                <Image style={{ width: '100%', height: 200, }} source={{ uri: DATA[0].img, }} />
            </View>

            <View style={{ flex: 1, backgroundColor: '#001F2D', marginBottom: 14, padding: 10 }}>
                <View style={{marginBottom : 5}}>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{DATA[0].description1}</Text>
                </View>

                <View style={{marginBottom : 5}}>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{DATA[0].description2}</Text>
                </View>

                <View style={{marginBottom : 5}}>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{DATA[0].description3}</Text>
                </View>
                

                <View style={{ marginTop: 10 }}>
                    <TouchableOpacity>
                        <Text style={{ color: '#F3B502', fontSize: 14, fontWeight: 'bold' }}>SEE FULL ARTICLE ON SCREEN RANT</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>

    )
}