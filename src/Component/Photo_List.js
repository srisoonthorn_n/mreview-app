import React from 'react'
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';

export default function Photo_List(props) {

    const DATA = [
        {
            id: '1',
            img: 'https://www.joblo.com/assets/images/joblo/news/2020/05/scoob-3.jpg',
        },
        {
            id: '2',
            img: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
        },
        {
            id: '3',
            img: 'https://static.rogerebert.com/uploads/review/primary_image/reviews/scoob-movie-review-2020/scoob-movie-review-2020.jpeg',
        },
        {
            id: '4',
            img: 'https://s.abcnews.com/images/Entertainment/WireAP_8e85f1599c0540e78439f5726953f704_16x9_992.jpg',
        },
        {
            id: '5',
            img: 'https://www.joblo.com/assets/images/joblo/news/2020/05/scoob-3.jpg',
        },
        {
            id: '6',
            img: 'https://www.joblo.com/assets/images/joblo/news/2020/05/scoob-3.jpg',
        },
        {
            id: '7',
            img: 'https://www.joblo.com/assets/images/joblo/news/2020/05/scoob-3.jpg',
        },
        {
            id: '8',
            img: 'https://www.joblo.com/assets/images/joblo/news/2020/05/scoob-3.jpg',
        },
        {
            id: '9',
            img: 'https://www.joblo.com/assets/images/joblo/news/2020/05/scoob-3.jpg',
        },
        {
            id: '10',
            img: 'https://www.joblo.com/assets/images/joblo/news/2020/05/scoob-3.jpg',
        },
        {
            id: '11',
            img: 'https://www.joblo.com/assets/images/joblo/news/2020/05/scoob-3.jpg',
        },
    ];

    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }}>

            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 14, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Text>LOGO</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ flex: 1, backgroundColor: '#001F2D' }}>
                <View style={{ flexDirection: 'row', height: 100, marginBottom: 5 }}>
                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity onPress={() => props.navigation.navigate('Photo_Show')}>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[0].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[1].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[2].img, }} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', height: 100, marginBottom: 5 }}>
                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[3].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[4].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[5].img, }} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', height: 100, marginBottom: 5 }}>
                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[6].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[7].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[8].img, }} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', height: 100, marginBottom: 5 }}>
                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[0].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[1].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[2].img, }} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', height: 100, marginBottom: 5 }}>
                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[0].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[1].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[2].img, }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', height: 100, marginBottom: 5 }}>
                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[0].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[1].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[2].img, }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', height: 100, marginBottom: 5 }}>
                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[0].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[1].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[2].img, }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', height: 100, marginBottom: 5 }}>
                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[0].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[1].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[2].img, }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', height: 100, marginBottom: 5 }}>
                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[0].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[1].img, }} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: 137 }}>
                        <TouchableOpacity>
                            <Image style={{ width: '100%', height: 100, }} source={{ uri: DATA[2].img, }} />
                        </TouchableOpacity>
                    </View>
                </View>

        

            </View>
        </ScrollView>
    )
}