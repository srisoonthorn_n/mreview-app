import React, { useState } from 'react'
import { View, Text, ScrollView, FlatList, SafeAreaView, StyleSheet, Image, TouchableOpacity, ImageBackground, TouchableNativeFeedback, TouchableWithoutFeedback, Animated } from 'react-native'
import { Card, WingBlank } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import AppIntroSlider from 'react-native-app-intro-slider';


const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Scoob!',
        date: '15 May 2020',
        img: 'https://m.media-amazon.com/images/M/MV5BYWY0OGIzNzgtNTRmZS00OTc1LWFmOWItYzM0NDc4OWQ3MGI3XkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg',
        imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
        trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo'
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Artemis Fowl',
        date: '29 May 2020',
        img: 'https://cdn.majorcineplex.com/uploads/movie/2573/thumb_2573.jpg',
        imgbg: 'https://cdn.theouterhaven.net/wp-content/uploads/2020/03/Artemis-Fowl-Trailer.png'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Dream Horse',
        date: '17 April 2020',
        img: 'https://steemitimages.com/p/6VvuHGsoU2QBt9MXeXNdDuyd4Bmd63j7zJymDTWgdcJjo1XyfYejg52JhAjoAEKd2kUaVebiNGu2RQPWrbz93TUFtbE7kqcoscc9Frm4sAY1cNK1xQqaTfCqqKGSgN?format=match&mode=fit&width=640',
        imgbg: 'https://s.yimg.com/ny/api/res/1.2/T.rewHjPw3AT1I.F9w5VGQ--~A/YXBwaWQ9aGlnaGxhbmRlcjtzbT0xO3c9ODAw/https://media.zenfs.com/en/variety.com/3c932ce704da2ca736fc6efe13c82aeb'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d82',
        title: 'The Wretched',
        date: '1 May 2020',
        img: 'https://lh3.googleusercontent.com/proxy/ZW-LoRu9JpU7ObR0sAl__Y9k_SS8TzSqkZgbuHOcI5OPD_ORYXl9KXKISt2hIJIGyeB-W6bVmCtA3Sv-LvflWJP0KWtfhoMm-wn6tptB',
        imgbg: 'https://i0.wp.com/bloody-disgusting.com/wp-content/uploads/2019/07/Screen-Shot-2019-07-05-at-8.38.05-AM.png?fit=1151%2C674&ssl=1'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e21d72',
        title: 'Free Guy',
        date: '11 December 2020',
        img: 'https://www.blognone.com/sites/default/files/externals/211e00c56ed01f65c46e9cfea8f30410.jpg',
        imgbg: 'https://m.media-amazon.com/images/M/MV5BMWJlZmFiNWItYTkyMy00NDE3LWI1ZWEtZjEwNTIxYWZjODQ1XkEyXkFqcGdeQW1hcmNtYW5u._V1_.jpg'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e21gf2',
        title: 'Coffee & Kareem',
        date: ' 3 April 2020',
        img: 'https://www.movie123hd.com/wp-content/uploads/2020/04/tt9898858-scaled.jpg',
        imgbg: 'https://www.indiewire.com/wp-content/uploads/2020/04/Coffee-Kareem-3.jpg'
    },
];





export default function Movie(props) {
    return (

        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }} >

            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 5, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Text>LOGO</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>

            </View>

            <View style={{ height: 40, backgroundColor: '#001F2D', flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Text style={{ color: '#9F9F9F' }}>HOME</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.itemMenu, styles.itemSelect]}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Movie')}>
                        <Text style={{ fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' }}>MOVIE</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Recommend')}>
                        <Text style={{ color: '#9F9F9F' }}>RECOMMENDATIONS</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ height: 320, backgroundColor: '#001F2D', marginBottom: 10 }}>
                <View style={{ flex: 1 }}>
                    {/* <FlatList 
                     data={DATA}
                     horizontal={true}
                     renderItem={({ item }) => (
                        <ImageBackground source={{ uri: item.imgbg }} style={{ flex: 1 }}>

                            <View style={{ flex: 1, alignItems: 'flex-end', flexDirection: 'row' }}>
                                <View style={{ backgroundColor: '#161616', height: 50, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Image source={{ uri: item.img }} style={{ height: 180, width: 120, marginBottom: 150, marginStart: 30 }} />
                                </View>
                                <View style={{ backgroundColor: '#161616', height: 50, width: 300, alignItems: 'flex-start', justifyContent: 'center' }}>
                     <Text style={{ color: '#FFFFFF', marginEnd: 50, fontSize: 18, marginStart: 30 }}>{item.title}</Text>
                                </View>
                            </View>
                        </ImageBackground>
                     )}
                    /> */}
                    <AppIntroSlider
                        data={DATA}
                        activeDotStyle={{ color: 'rgba(0 0 0 0)' }}
                        dotStyle={{ color: 'rgba(0 0 0 0)' }}
                        showNextButton={false}
                        showDoneButton={false}
                        renderItem={({ item }) => (

                            <ImageBackground source={{ uri: item.imgbg }} style={{ flex: 1 }}>

                                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => props.navigation.navigate('Video')}>
                                        <Icon name='play-circle' size={64} color='#FFFFFF' />
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1, alignItems: 'flex-end', flexDirection: 'row' }}>

                                    <View style={{ backgroundColor: '#161616', height: 50, alignItems: 'flex-start', justifyContent: 'center' }}>

                                        <Image source={{ uri: item.img }} style={{ height: 180, width: 120, marginBottom: 150, marginStart: 20 }} />

                                    </View>


                                    <View style={{ backgroundColor: '#161616', height: 50, width: 300, alignItems: 'flex-start', justifyContent: 'center' }}>
                                        <Text style={{ color: '#FFFFFF', marginEnd: 50, fontSize: 18, marginStart: 30 }}>{item.title}</Text>
                                    </View>

                                </View>


                            </ImageBackground>

                        )}
                    />

                </View>

            </View>

            <View style={{ height: 350, backgroundColor: '#001F2D', marginBottom: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Coming Soon</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#F3B502', margin: 10, fontSize: 15, fontWeight: 'bold' }}>SEE ALL</Text>
                        </TouchableOpacity>

                    </View>

                </View>

                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={DATA}
                        horizontal={true}
                        renderItem={({ item }) => (
                            <Card style={{ width: 180, height: 280, marginLeft: 10, marginRight: 10, }}>
                                {/* <Card.Header
                                    thumb ={item.img}
                                    thumbStyle={{width: 170, height: 250 }} 
                                    /> */}
                                <Image
                                    style={{ width: '100%', height: 208 }}
                                    source={{
                                        uri: item.img,
                                    }}
                                />
                                <View style={{ height: 70, paddingLeft: 10, backgroundColor: '#161616' }}>
                                    <View style={{ flex: 1, padding: 5 }}>
                                        <Text style={{ color: '#FFFFFF', fontSize: 18, fontWeight: 'bold' }}>{item.title}</Text>
                                    </View>
                                    <View style={{ flex: 1, padding: 5 }}>
                                        <Text style={{ color: '#FFFFFF', fontSize: 12, }}>{item.date}</Text>
                                    </View>
                                </View>


                            </Card>
                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>

            </View>

            <View style={{ height: 350, backgroundColor: '#001F2D', marginBottom: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Popular Movie</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#F3B502', margin: 10, fontSize: 15, fontWeight: 'bold' }}>SEE ALL</Text>
                        </TouchableOpacity>

                    </View>

                </View>

                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={DATA}
                        horizontal={true}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => props.navigation.navigate('Movie_Detail')}>
                            <Card style={{ width: 180, height: 280, marginLeft: 10, marginRight: 10, }}>
                                {/* <Card.Header
                                    thumb ={item.img}
                                    thumbStyle={{width: 170, height: 250 }} 
                                    /> */}
                                <Image
                                    style={{ width: '100%', height: 208 }}
                                    source={{
                                        uri: item.img,
                                    }}
                                />
                                <View style={{ height: 70, paddingLeft: 10, backgroundColor: '#161616' }}>
                                    <View style={{ flex: 1, padding: 5 }}>
                                        <Text style={{ color: '#FFFFFF', fontSize: 18, fontWeight: 'bold' }}>{item.title}</Text>
                                    </View>
                                    <View style={{ flex: 1, padding: 5 }}>
                                        <Text style={{ color: '#FFFFFF', fontSize: 12, }}>{item.date}</Text>
                                    </View>
                                </View>


                            </Card>
                            </TouchableOpacity>
                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>

            </View>

            <View style={{ height: 350, backgroundColor: '#001F2D', marginBottom: 20 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Top Rate Movie</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                        <TouchableOpacity>
                            <Text style={{ color: '#F3B502', margin: 10, fontSize: 15, fontWeight: 'bold' }}>SEE ALL</Text>
                        </TouchableOpacity>

                    </View>

                </View>

                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={DATA}
                        horizontal={true}
                        renderItem={({ item }) => (
                            <Card style={{ width: 180, height: 280, marginLeft: 10, marginRight: 10, }}>
                                {/* <Card.Header
                                    thumb ={item.img}
                                    thumbStyle={{width: 170, height: 250 }} 
                                    /> */}
                                <Image
                                    style={{ width: '100%', height: 208 }}
                                    source={{
                                        uri: item.img,
                                    }}
                                />
                                <View style={{ height: 70, paddingLeft: 10, backgroundColor: '#161616' }}>
                                    <View style={{ flex: 1, padding: 5 }}>
                                        <Text style={{ color: '#FFFFFF', fontSize: 18, fontWeight: 'bold' }}>{item.title}</Text>
                                    </View>
                                    <View style={{ flex: 1, padding: 5 }}>
                                        <Text style={{ color: '#FFFFFF', fontSize: 12, }}>{item.date}</Text>
                                    </View>
                                </View>


                            </Card>
                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>

            </View>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#F3B502'
    },
});