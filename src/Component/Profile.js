import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function Profile(props) {
    return (
        <View style={styles.container}>
            <View style={styles.viewHeader}>

                <View style={styles.viewLogo}>
                    <Text>LOGO</Text>
                </View>

                <View style={styles.viewProfile}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>

            </View>

            <TouchableOpacity>
                <View style={styles.viewList}>

                    <View style={styles.viewIconList}>
                        <Icon name='tasks' size={32} color='#FFFFFF' solid></Icon>
                    </View>

                    <View style={styles.viewTextList}>
                        <Text style={styles.TextList}>Your List</Text>
                    </View>
                </View>
            </TouchableOpacity>

            <TouchableOpacity>
                <View style={styles.viewList}>
                    <View style={styles.viewIconList}>
                        <Icon name='pen-square' size={32} color='#FFFFFF' solid></Icon>
                    </View>

                    <View style={styles.viewTextList}>
                        <Text style={styles.TextList}>Review</Text>
                    </View>
                </View>
            </TouchableOpacity>

            <TouchableOpacity>
                <View style={styles.viewList}>
                    <View style={styles.viewIconList}>
                        <Icon name='star' size={32} color='#FFFFFF' solid></Icon>
                    </View>

                    <View style={styles.viewTextList}>
                        <Text style={styles.TextList}>Ratings</Text>
                    </View>
                </View>
            </TouchableOpacity>

            <TouchableOpacity>
                <View style={styles.viewList}>
                    <View style={styles.viewIconList}>
                        <Icon name='cogs' size={32} color='#FFFFFF' solid></Icon>
                    </View>

                    <View style={styles.viewTextList}>
                        <Text style={styles.TextList}>Setting</Text>
                    </View>
                </View>
            </TouchableOpacity>

            <View style={styles.viewButton}>
                <TouchableOpacity onPress={() => props.navigation.navigate('Login')} style={styles.button}>
                    <View style={styles.viewBodyButton}>
                        <View style={styles.viewBodyIconButton}>
                            <Icon name='sign-in-alt' size={32} color='#FFFFFF' solid></Icon>
                        </View>

                        <View style={styles.textButton}>
                            <Text style={styles.TextList}>Login</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>



        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#161616',
        flex: 1
    },
    viewHeader: {
        height: 40,
        backgroundColor: '#FFFFFF',
        marginBottom: 10,
        flexDirection: 'row'
    },
    viewLogo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginStart: 20
    },
    viewProfile: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginEnd: 20
    },
    viewList: {
        width: '100%',
        height: 56,
        backgroundColor: '#001F2D',
        marginBottom: 5,
        flexDirection: 'row'
    },
    viewIconList: {
        flex: 0,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 30
    },
    viewTextList: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center'
    },
    TextList: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: 'bold'
    },
    viewButton: {
        height: 350,
        margin: 50,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    button: {
        width: '100%'
    },
    viewBodyButton: {
        width: '80%',
        height: 64,
        backgroundColor: '#009B38',
        borderRadius: 20,
        marginBottom: 5,
        flexDirection: 'row',
    },
    viewBodyIconButton: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 30,
    },
    textButton: {
        flex: 3,
        alignContent: 'center',
        justifyContent: 'center'
    }

});