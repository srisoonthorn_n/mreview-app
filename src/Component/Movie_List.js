import React from 'react'
import { View, ScrollView, TouchableOpacity, Text, SafeAreaView, FlatList, StyleSheet, Image } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { Card } from '@ant-design/react-native';

const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Scoob!',
        date: '15 May 2020',
        img: 'https://m.media-amazon.com/images/M/MV5BYWY0OGIzNzgtNTRmZS00OTc1LWFmOWItYzM0NDc4OWQ3MGI3XkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg',
        imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
        trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo',
        description: 'Scooby and the gang face their most challenging mystery ever: a plot to unleash the ghost dog Cerberus upon the world.As they race to stop this dogpocalypse, the gang discovers that Scooby has an epic destiny greater than anyone imagined.'
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Artemis Fowl',
        date: '29 May 2020',
        img: 'https://cdn.majorcineplex.com/uploads/movie/2573/thumb_2573.jpg',
        imgbg: 'https://cdn.theouterhaven.net/wp-content/uploads/2020/03/Artemis-Fowl-Trailer.png',
        description: 'Artemis Fowl, a young criminal prodigy, hunts down a secret society of fairies to find his missing father.'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Dream Horse',
        date: '17 April 2020',
        img: 'https://steemitimages.com/p/6VvuHGsoU2QBt9MXeXNdDuyd4Bmd63j7zJymDTWgdcJjo1XyfYejg52JhAjoAEKd2kUaVebiNGu2RQPWrbz93TUFtbE7kqcoscc9Frm4sAY1cNK1xQqaTfCqqKGSgN?format=match&mode=fit&width=640',
        imgbg: 'https://s.yimg.com/ny/api/res/1.2/T.rewHjPw3AT1I.F9w5VGQ--~A/YXBwaWQ9aGlnaGxhbmRlcjtzbT0xO3c9ODAw/https://media.zenfs.com/en/variety.com/3c932ce704da2ca736fc6efe13c82aeb',
        description: 'Dream Alliance, an unlikely race horse bred by small town Welsh bartender, Jan Vokes. With no experience, Jan convinces her neighbors to chip in their meager earnings to help raise Dream in the hopes he can compete with the racing elites.'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d82',
        title: 'The Wretched',
        date: '1 May 2020',
        img: 'https://lh3.googleusercontent.com/proxy/ZW-LoRu9JpU7ObR0sAl__Y9k_SS8TzSqkZgbuHOcI5OPD_ORYXl9KXKISt2hIJIGyeB-W6bVmCtA3Sv-LvflWJP0KWtfhoMm-wn6tptB',
        imgbg: 'https://i0.wp.com/bloody-disgusting.com/wp-content/uploads/2019/07/Screen-Shot-2019-07-05-at-8.38.05-AM.png?fit=1151%2C674&ssl=1',
        description: 'A defiant teenage boy, struggling with his parent\'\s imminent divorce, faces off with a thousand year-old witch, who is living beneath the skin of and posing as the woman next door.'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e21d72',
        title: 'Free Guy',
        date: '11 December 2020',
        img: 'https://www.blognone.com/sites/default/files/externals/211e00c56ed01f65c46e9cfea8f30410.jpg',
        imgbg: 'https://m.media-amazon.com/images/M/MV5BMWJlZmFiNWItYTkyMy00NDE3LWI1ZWEtZjEwNTIxYWZjODQ1XkEyXkFqcGdeQW1hcmNtYW5u._V1_.jpg',
        description: 'A bank teller discovers that he\'\s actually an NPC inside a brutal, open world video game.'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e21gf2',
        title: 'Coffee & Kareem',
        date: ' 3 April 2020',
        img: 'https://www.movie123hd.com/wp-content/uploads/2020/04/tt9898858-scaled.jpg',
        imgbg: 'https://www.indiewire.com/wp-content/uploads/2020/04/Coffee-Kareem-3.jpg',
        description: 'Twelve-year-old Kareem Manning hires a criminal to scare his mom\'\s new boyfriend -police officer James Coffee - but it backfires, forcing Coffee and Kareem to team up in order to save themselves from Detroit\'\s most ruthless drug kingpin.'
    },
];


export default function Movie_List(props) {
    return (
        <View style={{ flex: 1, backgroundColor: '#0C0C0C' }} contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40, backgroundColor: '#FFFFFF', flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Text>LOGO</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ flex: 1, backgroundColor: '#001F2D', marginTop: 10 }}>
                <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Popular Movie</Text>
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={DATA}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => props.navigation.navigate('Movie_Detail')} >

                                <View style={{ height: 200, width: '100%', backgroundColor: '#161616', flexDirection: 'row', marginBottom: 7 }}>
                                    <View style={{ flex: 0, margin: 10 ,justifyContent : 'center' }}>
                                        <Image source={{ uri: item.img }}
                                            style={{ width: 105, height: 150 , borderRadius : 3}}></Image>
                                    </View>

                                    <View style={{ flex: 1, padding : 10}}>
                                        <View style={{ flex: 0, margin: 5 }}>
                                            <Text style={{ color: '#FFFFFF', fontSize: 14, fontWeight: 'bold' }}>{item.title}</Text>
                                        </View>

                                        <View style={{ flex: 0, margin: 5 }}>
                                            <Text style={{ color: '#BDBDBD', fontSize: 12 }}>{item.description}
                                            </Text>
                                        </View>

                                        <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-end' , marginBottom : 15 }}>
                                            <View style={{ flexDirection: 'row' , alignItems : 'center' }}>
                                                <View>
                                                    <Icon name='star' size={24} color='#F4BC00' solid></Icon>
                                                </View>
                                                <View style={{marginStart : 5}}>
                                                    <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>7.0</Text>
                                                </View>
                                            </View>

                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>

                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#001F2D'
    }
});