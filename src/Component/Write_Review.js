import React from 'react'
import { View, Text, ScrollView, ImageBackground, Image, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SelectInput from 'react-native-select-input-ios';
import { AirbnbRating } from 'react-native-ratings';


export default function Write_Review(props) {

    const DATA = [
        {
            id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
            title: 'Scoob!',
            date: '15 May 2020',
            img: 'https://m.media-amazon.com/images/M/MV5BYWY0OGIzNzgtNTRmZS00OTc1LWFmOWItYzM0NDc4OWQ3MGI3XkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_.jpg',
            imgbg: 'https://i.ytimg.com/vi/-xjfyOI3NFc/maxresdefault.jpg',
            trailer: 'https://www.youtube.com/watch?v=GzlEnS7MmUo'
        },
        {
            id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
            title: 'Artemis Fowl',
            date: '29 May 2020',
            img: 'https://cdn.majorcineplex.com/uploads/movie/2573/thumb_2573.jpg',
            imgbg: 'https://cdn.theouterhaven.net/wp-content/uploads/2020/03/Artemis-Fowl-Trailer.png'
        },
        {
            id: '58694a0f-3da1-471f-bd96-145571e29d72',
            title: 'Dream Horse',
            date: '17 April 2020',
            img: 'https://steemitimages.com/p/6VvuHGsoU2QBt9MXeXNdDuyd4Bmd63j7zJymDTWgdcJjo1XyfYejg52JhAjoAEKd2kUaVebiNGu2RQPWrbz93TUFtbE7kqcoscc9Frm4sAY1cNK1xQqaTfCqqKGSgN?format=match&mode=fit&width=640',
            imgbg: 'https://s.yimg.com/ny/api/res/1.2/T.rewHjPw3AT1I.F9w5VGQ--~A/YXBwaWQ9aGlnaGxhbmRlcjtzbT0xO3c9ODAw/https://media.zenfs.com/en/variety.com/3c932ce704da2ca736fc6efe13c82aeb'
        },
        {
            id: '58694a0f-3da1-471f-bd96-145571e29d82',
            title: 'The Wretched',
            date: '1 May 2020',
            img: 'https://lh3.googleusercontent.com/proxy/ZW-LoRu9JpU7ObR0sAl__Y9k_SS8TzSqkZgbuHOcI5OPD_ORYXl9KXKISt2hIJIGyeB-W6bVmCtA3Sv-LvflWJP0KWtfhoMm-wn6tptB',
            imgbg: 'https://i0.wp.com/bloody-disgusting.com/wp-content/uploads/2019/07/Screen-Shot-2019-07-05-at-8.38.05-AM.png?fit=1151%2C674&ssl=1'
        },
        {
            id: '58694a0f-3da1-471f-bd96-145571e21d72',
            title: 'Free Guy',
            date: '11 December 2020',
            img: 'https://www.blognone.com/sites/default/files/externals/211e00c56ed01f65c46e9cfea8f30410.jpg',
            imgbg: 'https://m.media-amazon.com/images/M/MV5BMWJlZmFiNWItYTkyMy00NDE3LWI1ZWEtZjEwNTIxYWZjODQ1XkEyXkFqcGdeQW1hcmNtYW5u._V1_.jpg'
        },
        {
            id: '58694a0f-3da1-471f-bd96-145571e21gf2',
            title: 'Coffee & Kareem',
            date: ' 3 April 2020',
            img: 'https://www.movie123hd.com/wp-content/uploads/2020/04/tt9898858-scaled.jpg',
            imgbg: 'https://www.indiewire.com/wp-content/uploads/2020/04/Coffee-Kareem-3.jpg'
        },
    ];

    const getPickerOptions = () => {
        return [
            { value: 0, label: 'Excited' },
            { value: 1, label: 'Thrilling' },
            { value: 2, label: 'Strange' },
        ];
    }
    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground blurRadius={7} source={{ uri: DATA[0].img }} style={{ flex: 1 }}>
                <View style={{ backgroundColor: 'rgba(0, 31, 46, 0.89)', flex: 1 }}>

                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start', marginStart: 20, marginTop: 30 }}>
                        <TouchableOpacity onPress={() => props.navigation.navigate('User_Review')}>
                            <Icon name='times-circle' size={24} color='#FFFFFF' solid></Icon>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, width: '100%', alignItems: 'center', marginBottom: 10 }}>
                        <Image
                            style={{ width: 200, height: 300 }}
                            source={{
                                uri: DATA[0].img,
                            }}
                        />
                    </View>

                    <View style={{ flex: 1, marginBottom: 15,marginTop : 10}}>
                        <View style={{ justifyContent: 'flex-start', alignItems : 'center' }}>
                            <Text style={{fontSize : 24 ,color : '#FFFFFF' , fontWeight : 'bold'}}>Total Rating</Text>
                            <View style={{ flexDirection: 'row' , marginTop : 10}}>
                                <View>
                                    <Text style={{ fontSize: 32, color: '#F3B502', fontWeight: 'bold' }}>10</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 32, color: '#FFFFFF' }}> / 10</Text>
                                </View>
                            </View>
                        </View>

                    </View>

                    <View style={{ flex: 1 }}>
                        <View style={{ marginStart: 20 }}>
                            <Text style={{ fontSize: 18, color: '#FFFFFF', fontWeight: 'bold' }}>Your Rating</Text>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <SelectInput value={0}
                                    options={getPickerOptions()}
                                    style={{ backgroundColor: '#FFFFFF', borderWidth: 1, borderColor: 'black', overflow: 'hidden', width: '70%', marginTop: 40, marginStart: 20 }} />
                            </View>
                            <View style={{ flex: 1 }}>
                                <View style={{ alignItems: 'center' }}>
                                    <AirbnbRating
                                        count={5}
                                        reviews={false}
                                        defaultRating={5}
                                        size={24}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={{ flex: 1, alignItems: 'center', margin: 20 }}>
                            <TouchableOpacity>
                                <Icon name='plus-circle' size={40} color='#FFFFFF' solid></Icon>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ flex: 1 }}>
                        <View style={{ marginStart: 20 }}>
                            <Text style={{ fontSize: 18, color: '#FFFFFF', fontWeight: 'bold' }}>Your Review</Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <View style={{ margin: 10 }}>
                                <TextInput
                                    placeholder='Write a Header for your review'
                                    placeholderTextColor='#595959'
                                    style={{
                                        backgroundColor: '#FFFFFF',
                                        borderBottomColor: '#000000',
                                        borderBottomWidth: 1,
                                    }} />
                            </View>

                            <View style={{ margin: 10 }}>
                                <TextInput
                                    maxLength={1000}
                                    multiline={true}
                                    placeholder='Write your review here'
                                    placeholderTextColor='#595959'
                                    style={{
                                        alignItems: 'flex-start',
                                        height: 120,
                                        backgroundColor: '#FFFFFF',
                                        borderBottomColor: '#000000',
                                        borderBottomWidth: 1,
                                    }} />
                            </View>

                        </View>
                    </View>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', margin: 25 }}>
                        <TouchableOpacity onPress={() => props.navigation.navigate('User_Review')}>
                            <Icon name='telegram' size={56} color='#FFFFFF' solid></Icon>
                        </TouchableOpacity>
                    </View>

                </View>
            </ImageBackground>
        </ScrollView>


    )
}